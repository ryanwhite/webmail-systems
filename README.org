#+TITLE: https://www.fsf.org/resources/webmail-systems
* What?
This repository serves as the latest version of useful text sitting at

[[https://www.fsf.org/resources/webmail-systems]]

Part of the background to this:
[[https://www.gnu.org/philosophy/javascript-trap.en.html]]

LibreJS:
[[https://www.gnu.org/software/librejs/]]

Basically, the question we're trying to answer is: "Is this provider running Java-script that is Libre friendly?"

It's only 1 file, and with not too many contributors, we don't anticipate
any version clashes or anything.

Pull requests welcome!

* Volunteer!
If you'd like to help with this page, and play your part in helping keep the web and tools that hang off it Libre, here's how to get started:
1) Install Firefox ([[https://www.mozilla.org/en-US/firefox/new/]])
2) Install LibreJS add-on ([[https://www.gnu.org/software/librejs/]])
3) Get an account with [[https://gitlab.com/]]
4) Contact ryan (at)cognician (dot)com, letting him know your account name, and you'd like to help with the webmail system.
5) Get assigned a ticket, which will contain instructions - but really, you'll be creating a mail account, and running basic tests, so we can classify the service provider.

Don't worry if you don't know how to use Emacs or Git/GitLab - that stuff can be learned, and we can help you out. What is important here is that you want to help!

* Checklist
When adding/rating a provider, here is a set of criteria, from most desired (top of list) to least (bottom of list)

- [ ] Can signup and use site with the LibreJS plugin installed?
- [ ] Can signup and use with JS disabled?
- [ ] Can conduct day-to-day use with LibreJS installed, or JS disabled, after having to register/pay using JS?
- [ ] Can POP/IMAP clients connect and operate, bypassing web interface

Red flags - criteria that immediately put the provider in the "Not recommended" list:

- [ ] Companies known to be owned by malicious state actors
- [ ] Companies known to have been compromised by the likes of PRISM
- [ ] Companies with a history of malicious activity against users.
